/*----------------------------------------------------------------------------*/
#include "my1apisrv.h"
/*----------------------------------------------------------------------------*/
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
/* size for static buffers */
#define HTTP_BUFFSIZE 4096
/*----------------------------------------------------------------------------*/
#define JSON_FLAG_INVALID "{\"flag\":false}"
/*----------------------------------------------------------------------------*/
int server_code(void* stuffs) {
	int flag, temp;
	char *command = 0x0, *chkpath = 0x0, *chkhttp = 0x0;
	my1conn_t *pcon = (my1conn_t*) stuffs;
	my1socksrv_t *psrv = (my1socksrv_t*)pcon->data;
	my1apisrv_t *papi = (my1apisrv_t*) psrv->data;
	/* event */
	if (papi->on_connect)
		papi->on_connect(papi,pcon,0x0);
	/* clear text - just reset... should we really free mem? */
	cstr_null(&papi->text);
	cstr_null(&papi->buff);
	/* loop to get all request data */
	while (1) {
		temp = sock_read(pcon->sock,papi->ibuf.size,(void*)papi->ibuf.buff);
		if (temp>0) {
			papi->ibuf.buff[temp] = 0x0; /* just in case! */
			cstr_append(&papi->text,papi->ibuf.buff);
			if (temp<papi->ibuf.size) break;
		}
		else if (temp<0) {
			if (errno!=EAGAIN&&errno!=EWOULDBLOCK) {
				/* event */
				if (papi->on_chk_errno)
					papi->on_chk_errno(papi,pcon,(void*)&errno);
				return 0;
			}
		}
		else break;
	}
	/* check input buffer */
	if (!papi->text.fill) {
		/* event */
		if (papi->on_zero_data)
			papi->on_zero_data(papi,pcon,0x0);
		return 0;
	}
	flag = 0; /* set to non-zero to stop server loop */
	/* process token(s) */
	papi->find.delim = COMMON_DELIM;
	strtok_prep(&papi->find,&papi->text);
	if (strtok_next(&papi->find)) command = strdup(papi->find.token.buff);
	if (strtok_next(&papi->find)) chkpath = strdup(papi->find.token.buff);
	if (strtok_next(&papi->find)) chkhttp = strdup(papi->find.token.buff);
	/* only support HTTP/1.1 ? => GET FILE HTTP/1.1 */
	if (!command||!chkpath||!chkhttp||chkpath[0]!='/'||
		strncmp(chkhttp,"HTTP",4)||
		(strncmp(command,"GET",4)&&strncmp(command,"POST",5))) {
		/* event */
		if (papi->on_error)
			papi->on_error(papi,pcon,0x0);
		/* ignore unsupported http requests */
	}
	else {
		/* look for http body! */
		temp = cstr_findwd(&papi->text,"\r\n\r\n");
		temp += 4;
		papi->pstr = &papi->text.buff[temp];
		if (temp<papi->text.fill-2) {
			papi->jreq = (my1json_t*) malloc(sizeof(my1json_t));
			if (papi->jreq) {
				json_init(papi->jreq);
				if (!json_from_str(papi->jreq,papi->pstr)) {
					json_free(papi->jreq);
					free((void*)papi->jreq);
					papi->jreq = 0x0;
				}
			}
		}
		/* prepare processing */
		papi->pcmd = command;
		papi->pstr = chkpath;
		/* event */
		if (papi->on_request)
			papi->on_request(papi,pcon,(void*)chkpath);
		else {
			char* pstr;
			my1json_t* pchk;
			/** must be handled here if no event handler provided! */
			papi->json.type = JSON_ASSOC;
			json_add_keyval(&papi->json,"flag","true");
			/* process chkpath */
			pchk = json_add_array(&papi->json,JSON_ASSOC);
			json_set_keyval(pchk,"request",0x0);
			cstr_null(&papi->buff);
			cstr_append(&papi->buff,chkpath);
			papi->find.delim = "/";
			strtok_prep(&papi->find,&papi->buff);
			while (strtok_next(&papi->find)) {
				snprintf(papi->obuf.buff,papi->obuf.size,
					"Param#%d",papi->find.tstep);
				json_add_keyval(pchk,papi->obuf.buff,papi->find.token.buff);
			}
			/* tag in request json */
			if (papi->jreq&&json_is_valid(papi->jreq)) {
				json_add_object(&papi->json,papi->jreq);
				json_set_keyval(papi->jreq,"message",0x0);
				papi->jreq = 0x0;
			}
			/* prepare reply */
			cstr_null(&papi->text);
			pstr = json_make_str(&papi->json);
			if (pstr) cstr_append(&papi->text,HTTP_HEAD_OK);
			else cstr_append(&papi->text,HTTP_HEAD_KO);
			if (pstr) {
				cstr_append(&papi->text,HTTP_HEAD_TYPE);
				temp = strlen(pstr);
				snprintf(papi->obuf.buff,papi->obuf.size,HTTP_HEAD_CLEN,temp);
				cstr_append(&papi->text,papi->obuf.buff);
			}
			cstr_append(&papi->text,HTTP_HEAD_ENDS);
			if (pstr) {
				cstr_append(&papi->text,pstr);
				free((void*)pstr);
			}
			sock_send(pcon->sock,papi->text.fill,(void*)papi->text.buff);
		}
		/* done with this! */
		json_free(&papi->json);
		/* just in case */
		if (papi->jreq) {
			json_free(papi->jreq);
			free((void*)papi->jreq);
			papi->jreq = 0x0;
		}
	}
	/* free strdup-created strings */
	free((void*)command);
	free((void*)chkpath);
	free((void*)chkhttp);
	/* done */
	return flag;
}
/*----------------------------------------------------------------------------*/
int server_done(void* stuffs) {
	my1conn_t *pcon = (my1conn_t*) stuffs;
	my1socksrv_t *psrv = (my1socksrv_t*)pcon->data;
	my1apisrv_t *papi = (my1apisrv_t*) psrv->data;
	if (papi->on_loop) {
		if (papi->on_loop(papi,pcon,0x0)) {
			if (papi->on_abort)
				papi->on_abort(papi,pcon,0x0);
			return -1; /* return non-zero to abort server loop */
		}
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
void apisrv_init(my1apisrv_t* apisrv,int port) {
	cstr_init(&apisrv->text);
	cstr_init(&apisrv->buff);
	cstr_init(&apisrv->ibuf);
	cstr_resize(&apisrv->ibuf,HTTP_BUFFSIZE);
	cstr_init(&apisrv->obuf);
	cstr_resize(&apisrv->obuf,HTTP_BUFFSIZE);
	strtok_init(&apisrv->find,COMMON_DELIM);
	json_init(&apisrv->json);
	apisrv->jreq = 0x0;
	socksrv_init(&apisrv->server,port);
	apisrv->pcmd = 0x0;
	apisrv->pstr = 0x0;
	apisrv->data = 0x0;
	apisrv->on_exec = 0x0;
	apisrv->on_done = 0x0;
	apisrv->on_none = 0x0;
	apisrv->on_loop = 0x0;
	apisrv->on_connect = 0x0;
	apisrv->on_abort = 0x0;
	apisrv->on_request = 0x0;
	apisrv->on_error = 0x0;
	apisrv->on_zero_data = 0x0;
	apisrv->on_chk_errno = 0x0;
	/* initialize server */
	apisrv->server.code = &server_code;
	apisrv->server.done = &server_done;
	apisrv->server.data = (void*) apisrv;
}
/*----------------------------------------------------------------------------*/
void apisrv_free(my1apisrv_t* apisrv) {
	socksrv_free(&apisrv->server);
	json_free(&apisrv->json);
	strtok_free(&apisrv->find);
	cstr_free(&apisrv->text);
	cstr_free(&apisrv->buff);
	cstr_free(&apisrv->ibuf);
	cstr_free(&apisrv->obuf);
}
/*----------------------------------------------------------------------------*/
void apisrv_exec(my1apisrv_t* apisrv) {
	socksrv_prep(&apisrv->server);
	if (apisrv->server.flag!=SERVER_READY) {
		if (apisrv->on_none)
			apisrv->on_none(apisrv,0x0,0x0);
		return;
	}
	if (apisrv->on_exec)
		apisrv->on_exec(apisrv,0x0,0x0);
	socksrv_exec(&apisrv->server);
	if (apisrv->on_done)
		apisrv->on_done(apisrv,0x0,0x0);
}
/*----------------------------------------------------------------------------*/
