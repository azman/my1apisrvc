/*----------------------------------------------------------------------------*/
#ifndef __MY1APISRV_H__
#define __MY1APISRV_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1http API server library
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
#include "my1socksrv.h"
#ifndef STR_TOKEN_UTIL
#define STR_TOKEN_UTIL
#endif
#include "my1cstr.h"
#include "my1cstr_util.h"
#include "my1strtok.h"
#ifndef JSON_FILE_STREAM
#define JSON_FILE_STREAM
#endif
#include "my1json.h"
/*----------------------------------------------------------------------------*/
#define MY1_HTTP_API_PORT 1337
/*----------------------------------------------------------------------------*/
#define HTTP_HEAD_OK "HTTP/1.1 200 OK"
#define HTTP_HEAD_KO "HTTP/1.1 404 Not Found"
#define HTTP_HEAD_TYPE "\r\nContent-Type: application/json"
#define HTTP_HEAD_CLEN "\r\nContent-Length: %d"
#define HTTP_HEAD_ENDS "\r\n\r\n"
/*----------------------------------------------------------------------------*/
typedef int (*event_t)(void*,void*,void*); /* object, conn, temp */
/*----------------------------------------------------------------------------*/
typedef struct _my1apisrv_t {
	my1cstr_t text, buff, ibuf, obuf;
	my1strtok_t find;
	my1json_t json, *jreq;
	char *pcmd, *pstr; /* for on_request => http method & path */
	my1socksrv_t server;
	void *data; /* general purpose data */
	event_t on_exec, on_done, on_none; /* on_none when socket is unavailable */
	event_t on_loop; /* event run by server_done code */
	event_t on_connect, on_abort, on_request, on_error;
	/* events when no data received and errno is NOT (EAGAIN OR EWOULDBLOCK) */
	event_t on_zero_data, on_chk_errno;
} my1apisrv_t;
/*----------------------------------------------------------------------------*/
void apisrv_init(my1apisrv_t* apisrv,int port);
void apisrv_free(my1apisrv_t* apisrv);
void apisrv_exec(my1apisrv_t* apisrv);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
