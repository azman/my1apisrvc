/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "my1apisrv.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#define ERROR_ARGS_OPT (SOCKET_ERROR|0x01)
#define ERROR_ARGS_PAR (SOCKET_ERROR|0x02)
#define ERROR_ARGS_PORT (SOCKET_ERROR|0x04)
/*----------------------------------------------------------------------------*/
typedef struct _data_t {
	FILE* plog;
	int flag, port;
} data_t;
/*----------------------------------------------------------------------------*/
int api_loop(void* that,void* conn,void* temp) {
	(void)conn; (void)temp; (void)that;
	if (get_keyhit()==KEY_ESCAPE)
		return -1;
	return 0;
}
/*----------------------------------------------------------------------------*/
int api_connect(void* that,void* conn,void* temp) {
	(void)temp;
	my1apisrv_t* papi = (my1apisrv_t*) that;
	my1conn_t* pcon = (my1conn_t*) conn;
	data_t* data = (data_t*) papi->data;
	if (data->plog)
		fprintf(data->plog,"Request from {%s}!\n",pcon->addr);
	return 0;
}
/*----------------------------------------------------------------------------*/
int api_abort(void* that,void* conn,void* temp) {
	(void)conn; (void)temp;
	my1apisrv_t* papi = (my1apisrv_t*) that;
	data_t* data = (data_t*) papi->data;
	if (data->plog)
		fprintf(data->plog,"User Abort Requested!\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
int api_protocol_error(void* that,void* conn,void* temp) {
	(void)temp;
	my1apisrv_t* papi = (my1apisrv_t*) that;
	my1conn_t* pcon = (my1conn_t*) conn;
	data_t* data = (data_t*) papi->data;
	if (data->plog)
		fprintf(data->plog,"Invalid HTTP protocol from client:{%s}!\n",
			pcon->addr);
	return 0;
}
/*----------------------------------------------------------------------------*/
int api_request(void* that,void* conn,void* temp) {
	char* pstr;
	my1json_t* pchk;
	my1apisrv_t* papi = (my1apisrv_t*) that;
	my1conn_t* pcon = (my1conn_t*) conn;
	char* chkpath = (char*) temp;
	data_t* data = (data_t*) papi->data;
	int test = 0;
	if (data->plog) {
		fprintf(data->plog,"<QUERY>\n%s",papi->text.buff);
		fprintf(data->plog,"</QUERY>\n");
	}
	if (!strncmp(chkpath,"/test",5)) test = 1;
	papi->json.type = JSON_ASSOC;
	json_add_keyval(&papi->json,"flag","true");
	if (!test)
		json_add_keyval(&papi->json,"info","i got this!");
	else {
		json_add_keyval(&papi->json,"info","test");
		printf("@@ Debug:{%s}\n",chkpath);
	}
	/* process chkpath */
	if (!test) {
		pchk = json_add_array(&papi->json,JSON_ASSOC);
		json_set_keyval(pchk,"request",0x0);
	}
	else pchk = 0x0;
	cstr_null(&papi->buff);
	cstr_append(&papi->buff,chkpath);
	papi->find.delim = "/";
	strtok_prep(&papi->find,&papi->buff);
	while (strtok_next(&papi->find)) {
		snprintf(papi->obuf.buff,papi->obuf.size,
			"Param#%d",papi->find.tstep);
		if (pchk)
			json_add_keyval(pchk,papi->obuf.buff,papi->find.token.buff);
		else
			printf("@@ Param#%d:%s\n",papi->find.tstep,papi->find.token.buff);
	}
	/* tag in request json */
	if (!test&&papi->jreq&&json_is_valid(papi->jreq)) {
		json_add_object(&papi->json,papi->jreq);
		json_set_keyval(papi->jreq,"message",0x0);
		papi->jreq = 0x0;
	}
	/* prepare reply */
	cstr_null(&papi->text);
	pstr = json_make_str(&papi->json);
	if (pstr) cstr_append(&papi->text,HTTP_HEAD_OK);
	else cstr_append(&papi->text,HTTP_HEAD_KO);
	if (pstr) {
		int size;
		cstr_append(&papi->text,HTTP_HEAD_TYPE);
		size = strlen(pstr);
		snprintf(papi->obuf.buff,papi->obuf.size,HTTP_HEAD_CLEN,size);
		cstr_append(&papi->text,papi->obuf.buff);
	}
	cstr_append(&papi->text,HTTP_HEAD_ENDS);
	if (pstr) {
		cstr_append(&papi->text,pstr);
		free((void*)pstr);
	}
	sock_send(pcon->sock,papi->text.fill,(void*)papi->text.buff);
	if (data->plog) {
		fprintf(data->plog,"<REPLY>\n%s",papi->text.buff);
		fprintf(data->plog,"</REPLY>\n");
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int api_found_no_data(void* that,void* conn,void* temp) {
	(void)that; (void)temp;
	my1conn_t* pcon = (my1conn_t*) conn;
	printf("Cannot get data from {%s}!\n",pcon->addr);
	return 0;
}
/*----------------------------------------------------------------------------*/
int api_found_errno(void* that,void* conn,void* temp) {
	(void)that;
	my1conn_t* pcon = (my1conn_t*) conn;
	int *pval = (int*)temp;
	printf("Cannot get socket data from {%s}! (%d)\n",pcon->addr,*pval);
	return 0;
}
/*----------------------------------------------------------------------------*/
int api_exec(void* that,void* conn,void* temp) {
	(void)conn; (void)temp;
	my1apisrv_t* papi = (my1apisrv_t*) that;
	printf("Running API server @port %d!\n",papi->server.port);
	printf("Press <ESC> to exit.\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
int api_done(void* that,void* conn,void* temp) {
	(void)conn; (void)temp;
	my1apisrv_t* papi = (my1apisrv_t*) that;
	printf("Server code done with code %d!\n",papi->server.flag);
	return 0;
}
/*----------------------------------------------------------------------------*/
int api_none(void* that,void* conn,void* temp) {
	(void)conn; (void)temp;
	my1apisrv_t* papi = (my1apisrv_t*) that;
	printf("Cannot run API server! (%d)\n",papi->server.sock.flag);
	return 0;
}
/*----------------------------------------------------------------------------*/
void data_init(data_t* data, int argc, char* argv[]) {
	int loop;
	/* initial/default settings */
	data->plog = 0x0;
	data->flag = 0;
	data->port = MY1_HTTP_API_PORT;
	for (loop=1;loop<argc;loop++) {
		if (argv[loop][0]=='-'&&argv[loop][1]=='-') {
			if (!strcmp(&argv[loop][2],"port")) {
				if (++loop<argc)
					data->port = atoi(argv[loop]);
				else {
					printf("No value for --port given!\n");
					data->flag |= ERROR_ARGS_PORT;
				}
			}
			else if (!strcmp(&argv[loop][2],"logs")) {
				data->plog = stderr;
			}
			else {
				printf("Unknown option '%s'!\n",argv[loop]);
				data->flag |= ERROR_ARGS_OPT;
			}
		}
		else {
			printf("Unknown parameter '%s'!\n",argv[loop]);
			data->flag |= ERROR_ARGS_PAR;
		}
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	data_t data;
	my1apisrv_t apisrv;
	data_init(&data,argc,argv);
	if (data.flag<0) return data.flag;
	/* configure server */
	apisrv_init(&apisrv,data.port);
	apisrv.on_exec = api_exec;
	apisrv.on_done = api_done;
	apisrv.on_none = api_none;
	apisrv.on_loop = api_loop;
	apisrv.on_connect = api_connect;
	apisrv.on_abort = api_abort;
	apisrv.on_request = api_request;
	apisrv.on_error = api_protocol_error;
	apisrv.on_zero_data = api_found_no_data;
	apisrv.on_chk_errno = api_found_errno;
	apisrv.data = (void*) &data;
	/* exec */
	apisrv_exec(&apisrv);
	/* done */
	apisrv_free(&apisrv);
	return 0;
}
/*----------------------------------------------------------------------------*/
