# makefile for my1apisrvc

EXETOOL = my1apisrvc
OBJTOOL = my1sockbase.o my1socksrv.o my1cstr.o my1cstr_util.o my1strtok.o
OBJTOOL += my1list.o my1list_data.o my1keys.o my1json.o my1apisrv.o my1apitest.o

EXTPATH = ../my1codelib/src

CFLAG += -Wall -I$(EXTPATH) -DSTR_TOKEN_UTIL
CFLAG += -DMY1APP_PROGVERS=\"$(shell date +%Y%m%d)\"
CFLAG +=
LFLAG +=

RM = rm -f
CC = gcc -c
LD = gcc

.PHONY: debug all new clean

debug: CFLAG += -g -DMY1_DEBUG

all: $(EXETOOL)

new: clean all

debug: new

${EXETOOL}: $(OBJTOOL)
	$(LD) $(CFLAG) -o $@ $+ $(LFLAG)

%.o: src/%.c src/%.h
	$(CC) $(CFLAG) -o $@ $<

%.o: src/%.c
	$(CC) $(CFLAG) -o $@ $<

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) $(CFLAG) -o $@ $<

clean:
	-$(RM) $(EXETOOL) $(EXESEND) *.o
